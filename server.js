const express = require('express');
const app = express();
const router = require('./routing')
const path = require('path')


//load css
app.use('/chapter3', express.static(path.join(__dirname, 'chapter3')))
app.use('/chapter4', express.static(path.join(__dirname, 'chapter4')))


//app.set json print style
app.set('json spaces', 4);
//routing
app.use(router);


//listen req
app.listen(5000, () => {
    console.log('on port 5000')
});

