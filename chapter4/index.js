const result_span = document.getElementById("hasil");
const playerRock_div = document.getElementById("player_b")
const playerPaper_div = document.getElementById("player_k")
const playerScissors_div = document.getElementById("player_g")
const comRock_div = document.getElementById("com_b")
const comPaper_div = document.getElementById("com_k")
const comScissors_div = document.getElementById("com_g")
const refresh = document.getElementById("refresh");
const playerChoice = document.querySelector(".playerChoice")
const once = { once: true };

function getComputerChoice() {
    const choice = ['com_b', 'com_k', 'com_g'];
    const randomNumber = Math.floor(Math.random() * 3);
    const com = document.getElementById(choice[randomNumber]);
    com.className = "com";
    console.log(choice[randomNumber])
    return choice[randomNumber];
}

class DisplayObject {
    constructor(id) {
        this.id = id
        this.object = document.getElementById("hasil")
    }
    writeObject(imageURL, width, height) {
        this.object.innerHTML = `<img src='${imageURL}' width='${width}' height='${height}'>`;
    }
}


function win(player, computer) {
    const playerWin = new DisplayObject("playerWin");
    playerWin.writeObject('assets/img/player_win.png', 271, 166)
    result_span.className = "playerWin";
}

function lose(player, computer) {
    const playerLose = new DisplayObject("playerLose");
    playerLose.writeObject('assets/img/COM_WIN.png', 271, 166)
    result_span.className = "playerLose";
}
function draw(player, computer) {
    const playerDraw = new DisplayObject("playerDraw");
    playerDraw.writeObject('assets/img/DRAW.png', 271, 166)
    result_span.className = "playerDraw";
}

function game(userChoice) {
    const computerChoice = getComputerChoice();
    switch (userChoice + computerChoice) {
        case "player_bcom_g":
        case "player_kcom_b":
        case "player_gcom_k":
            win(userChoice, computerChoice)
            break;
        case "player_bcom_k":
        case "player_kcom_g":
        case "player_gcom_b":
            lose(userChoice, computerChoice)
            break;
        case "player_bcom_b":
        case "player_kcom_k":
        case "player_gcom_g":
            draw(userChoice, computerChoice)
    }
}

function main() {
    playerRock_div.addEventListener('click', () => game("player_b"), once);
    playerPaper_div.addEventListener('click', () => game("player_k"), once);
    playerScissors_div.addEventListener('click', () => game("player_g"), once);
    refresh.addEventListener('click', () => location.reload());
}

main();