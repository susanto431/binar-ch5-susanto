const express = require('express');
const users = require('./users_API/users.json');
const router = express.Router();

//chapter 3
router.get('/chapter3', (req, res) => {
    res.sendFile('./chapter3/index.html', { root: __dirname });
});

//chapter 4
router.get('/chapter4', (req, res) => {
    res.sendFile('./chapter4/handGame.html', { root: __dirname });
});

//user static
router.get('/users', (req, res) => {
    // sorting json object
    function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();

        let comparison = 0;
        if (nameA > nameB) {
            comparison = 1;
        } else if (nameA < nameB) {
            comparison = -1;
        }
        return comparison;
    }
    // users.users.sort(compare);
    res.json(users.users.sort(compare));
});

module.exports = router;